import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
// import axios from "axios";
import data from "./data.json"
export const getImages = createAsyncThunk(
    'imageSlice/getImages',
    async () => {
        return data
        //axios.get("./data.json").then(res => res.json());
        // return fetch('data.json', {
        //     headers: {
        //         'Content-Type': 'application/json',
        //         'Accept': 'application/json'
        //     }
        // }).
        //     then(res => res.json());

    }
);

export const imageSlice = createSlice({
    name: 'imageSlice',
    initialState: {
        value: [],
    },
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getImages.pending, (state) => {
                console.log('still loading please wait')
            })
            .addCase(getImages.fulfilled, (state, action) => {
                state.value = action.payload;
                console.log('state updated successfully')
                console.log(state.value)
            });
    },
})

export const selectImages = (state) => {
    return state.images.value
}


export default imageSlice.reducer
