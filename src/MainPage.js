
import React, { useState, useEffect } from 'react';
import { Container, Header, Slider, Navbar, Nav, Sidenav, Panel, Row, Col, Grid } from 'rsuite';
import { getImages, selectImages } from './reducers/image';
import { useSelector, useDispatch } from 'react-redux';
import TextField from "@mui/material/TextField";


const MainPage = () => {
    const [searchTerm, setSearchTerm] = useState('')
    const [imgCss, setImgCss] = useState({})
    const [imageData, setImageData] = useState({})
    const [orgImageTypeData, setIrgImageTypeData] = useState({})
    const [imageTypeData, setImageTypeData] = useState([])
    const dispatch = useDispatch();
    const selectImagesObj = useSelector(selectImages);

    const setOpacity = (e) => {
        setImgCss(
            { opacity: e.target.value }
        )
    }

    const submitHandler = (e) => {
        e.preventDefault();
        console.log(e.target.value)
        setSearchTerm(e.target.value);
        Object.entries(imageData).map((item, i) => {
            let filterArray = item[1].filter(innerItem => {
                return innerItem.text.toLowerCase().includes(e.target.value)
            });
            if (filterArray.length == 0) {
                setIrgImageTypeData(orgImageTypeData)
            } else {
                setImageTypeData(filterArray)
            }
        })


        // for (const [key, value] of Object.entries(imageData)) {

        // }
    }

    useEffect(() => {
        const newIm = async () => {
            dispatch(getImages());
            setImageData(selectImagesObj);
        }
        newIm();
        //setImageTypeData()
        if (Object.keys(imageData)[0]) {
            filterImage(Object.keys(imageData)[0]);
        }

    }, [imageData])




    const filterImage = (keyName) => {

        setImageTypeData(imageData[keyName])
        setIrgImageTypeData(imageData[keyName])
    }
    return (
        <div className="show-fake-browser login-page">
            <Container>
                <Header style={{ marginTop: '10' }}>


                    <Navbar >
                        <Grid fluid>
                            <Row className="show-grid">
                                <Col xs={24} sm={24} md={8}>

                                </Col>
                                <Col xs={24} sm={24} md={8}>
                                    <TextField
                                        id="outlined-basic"

                                        label="Search"
                                        value={searchTerm}
                                        onChange={submitHandler}
                                    />
                                </Col>
                                <Col xs={24} sm={24} md={8}>

                                </Col>
                            </Row>
                        </Grid>
                    </Navbar>
                </Header>
                <div className='parent-class'>


                    <div className='left-child-class'>
                        {imageData ? <Sidenav>
                            <Sidenav.Body>
                                <Nav activeKey="1">

                                    {Object.keys(imageData).map((item, i) => (
                                        <Nav.Item key={i} onClick={() => filterImage(item)}>
                                            {item}
                                        </Nav.Item>
                                    ))}

                                    <Nav.Item>
                                        <TextField
                                            variant="outlined"
                                            label="Opacity"
                                            type="number"

                                            onChange={setOpacity}
                                        />
                                    </Nav.Item>



                                </Nav>

                            </Sidenav.Body>
                        </Sidenav> : null}
                    </div>
                    <div className='right-child-class'>


                        <div>

                            {

                                imageTypeData ?
                                    imageTypeData.map((item, i) => {

                                        return (

                                            <Panel shaded bordered bodyFill style={{ display: 'inline-block', width: 240 }} key={i}>
                                                <img src={item.image} height="240" style={imgCss} />
                                                <Panel header={item.text}>

                                                </Panel>
                                            </Panel>
                                        )


                                    }) : null

                            }

                        </div>


                        {
                            imageTypeData ? <div>


                                {
                                    imageTypeData.map(item => {
                                        <div>{item.image}</div>


                                    })
                                }

                            </div> : null
                        }

                    </div>
                </div>
            </Container >
        </div >
    );
};

export default MainPage;