import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import MainPage from "./MainPage"
import store from './store';
import { Provider } from 'react-redux';
import 'rsuite/dist/rsuite.min.css'; // or 'rsuite/dist/rsuite.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <MainPage />
  </Provider>
);


